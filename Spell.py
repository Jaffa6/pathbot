from re import fullmatch, search, sub

class Spell:
	name = ''
	traits = []
	level = -1
	rarity = ''
	type = ''
	range = None
	area = None
	target = None
	duration = ''
	saveType = None
	save = None
	description = ''
	subsections = {}
	heightening = None
	components = []
	actions = ''
	
	def __init__(self, dataDict):
		# Normal .title() doesn't work well with apostrophes
		self.name = ' '.join(token.capitalize() for token in dataDict['name'].split(' '))
		
		self.traits = dataDict['traits']
		self.level = dataDict['level']
		self.rarity = convertRarity(dataDict['rarity'])
		self.type = dataDict['type']
		
		if 'range' in dataDict:
			self.range = dataDict['range']
			
		if 'area' in dataDict:
			self.area = dataDict['area']
			
		if 'target' in dataDict:
			self.target = dataDict['target']
			
		if 'duration' in dataDict:
			self.duration = dataDict['duration']
			
		if 'save' in dataDict:
			self.saveType = dataDict['save']
			
		self.description = dataDict['description']['main']
		if search(' \${[a-z\.1-9]+}', self.description):
			self.description = sub(' \${[a-z\.1-9]+}', '', self.description)
		
		if 'subsections' in dataDict['description']:
			self.subsections = dataDict['description']['subsections']
			if 'save' in self.subsections: # Handled elsewhere
				self.save = dataDict['description']['subsections']['save']
				del self.subsections['save']
				
		
		if 'heightened' in dataDict:
			self.heightening = dataDict['heightened']
			
		self.components = dataDict['cast']['components']
		if 'actions' in dataDict['cast']:
			self.actions = convertActions(dataDict['cast']['actions'])
		elif 'time' in dataDict['cast']:
			self.actions = dataDict['cast']['time']
	
	def __str__(self):
		return f'The spell {self.name} requires {self.actions} to cast and has the description: "{self.description}".'
	
def convertRarity(rarity):
	rarities = ['common', 'uncommon', 'rare']
	
	return rarities[rarity]

def convertActions(actions):
	if fullmatch('a to [a]+', actions):
		tokens = actions.split(' ')
		numActions = tokens[0].count('a')
		numExtendedActions = tokens[2].count('a')
		
		# a, aa, r, etc.
		# a or more
		# f (free action)
		
		return f'Between {numActions} and {numExtendedActions} Actions (inclusive)'
	
	elif fullmatch('[a]+', actions):
		count = actions.count('a')
		if count > 1:
			return f'{count} Actions'
		else:
			return f'a single Action'
		
	elif fullmatch('a or more', actions):
		return 'one or more Actions'
		
	elif actions == 'r':
		return 'a Reaction'
		
	elif actions == 'f':
		return 'a Free Action'
	
	else:
		raise ValueError(f'Unknown action string: {actions}')