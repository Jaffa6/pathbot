from json import loads
from re import fullmatch
from constants import savePath, authPath
from discord.ext import commands
from tabulate import tabulate
from random import choice, randint
from os import listdir
from os.path import join
from discord import File
from requests import get
from Levenshtein import distance

frogFacts = []
pikachuFacts = []
garfieldFacts = []
positivityLines = []

flavour = {}

def getSuffix(number):
	suffixes = {
		'st': [1],
		'nd': [2],
		'rd': [3],
		'th': [0, 4, 5, 6, 7, 8, 9, 10]
	}
	
	for suffix, values in suffixes.items():
		if number in values:
			return suffix

def generateFlavour(spellName):
	global flavour
	
	if len(flavour) == 0:
		with open('flavour.txt') as f:
			flavour = loads(f.read())
	
	start = choice(flavour['starts'])
	name = choice(choice(flavour['names']))
	title = choice(flavour['titles'])
	event = choice(flavour['events'])
	
	return f'{start} {name} the {title.title()} used {spellName.title()} to {event}.'

def editDistance(spellList, spellInput):
	if spellInput in spellList:
		return [(spellInput, 0)]
	
	matches = []
	lowest = len(spellInput)
	
	for spellCurrent in spellList:
		distanceValue = distance(spellInput, spellCurrent)
		threshold = max(3.0, len(spellCurrent)/4)
		if distanceValue <= lowest and distanceValue < threshold:
			matches.append((spellCurrent, distanceValue))
	
	if not len(matches):
		return [(None, -1)]
	else:
		return sorted(matches, key=lambda x: x[1])

def getRandomImage(directory):
	"""
	:param directory: The directory to look in
	:return: The path to a randomly-chosen picture in the given directory.
	"""
	pics = listdir(join(directory, 'images'))
	pic = choice([
		join(directory, 'images', pic) for pic in pics
	])
	
	return pic

def getQuotes(command, filename):
	"""
	Loads a JSON quotes/facts file.
	
	:param command The endpoint (and thus directory) to look in the quotes directory of
	:param filename: A filename in the directory indicate by [command/"quotes"]
	:return: The result of loading the JSON
	"""
	
	with open(join(command, 'quotes', filename)) as f:
		return loads(f.read())

with open(authPath) as authFile:
	token = loads(authFile.read())['token']
	print("Loaded the auth now")

with open(savePath) as spellFile:
	spellDict = loads(spellFile.read())
	print("Loaded the spells now")

bot = commands.Bot(command_prefix='!')
bot.remove_command('help')

@bot.command()
async def cat(ctx):
	print('Kitty Time!')
	
	kitty = getRandomImage('cats')
	
	request = get('https://some-random-api.ml/facts/cat')
	content = loads(request.content.decode())
	fact = content['fact']
	
	await ctx.send(file=File(kitty))
	await ctx.send(fact)

@bot.command()
async def dog(ctx):
	print('Doggy Time!')
	
	request = get('https://dog.ceo/api/breeds/image/random')
	content = loads(request.content.decode())
	url = content['message']
	
	request = get('https://some-random-api.ml/facts/dog')
	content = loads(request.content.decode())
	fact = content['fact']
	
	await ctx.send(f'''{url}

{fact}''')

@bot.command()
async def wolf(ctx):
	print('Wolfhound Time!')
	
	request = get('https://dog.ceo/api/breeds/image/random')
	content = loads(request.content.decode())
	url = content['message']
	
	request = get('https://some-random-api.ml/facts/dog')
	content = loads(request.content.decode())
	fact = content['fact']
	
	await ctx.send(f'''{url}

{fact}''')

@bot.command()
async def fox(ctx):
	print('Fluffy Time!')
	
	request = get('https://some-random-api.ml/img/fox')
	content = loads(request.content.decode())
	url = content['link']
	
	request = get('https://some-random-api.ml/facts/fox')
	content = loads(request.content.decode())
	fact = content['fact']
	
	await ctx.send(f'''{url}

{fact}''')

@bot.command()
async def bird(ctx):
	print('Birb Time!')
	
	request = get('https://some-random-api.ml/img/birb')
	content = loads(request.content.decode())
	url = content['link']
	
	request = get('https://some-random-api.ml/facts/bird')
	content = loads(request.content.decode())
	fact = content['fact']
	
	await ctx.send(f'''{url}

{fact}''')

@bot.command()
async def frog(ctx):
	global frogFacts
	
	if len(frogFacts) == 0:
		frogFacts = getQuotes('frogs', 'facts.json')
	
	print('Frog Time!')
	
	num = str(randint(1, 54)).zfill(4)
	
	fact = choice(frogFacts)
	
	await ctx.send(f'''{f'http://www.allaboutfrogs.org/funstuff/random/{num}.jpg'}

{fact}''')

@bot.command()
async def pikachu(ctx):
	global pikachuFacts
	
	if len(pikachuFacts) == 0:
		pikachuFacts = getQuotes('pikachu', 'facts.json')
	
	print('Pokemon Time!')
	
	request = get('https://some-random-api.ml/pikachuimg')
	content = loads(request.content.decode())
	url = content['link']
	
	quote = choice(pikachuFacts)
	
	await ctx.send(f'''{url}

{quote}''')

@bot.command()
async def garfield(ctx):
	global garfieldFacts
	
	if len(garfieldFacts) == 0:
		garfieldFacts = getQuotes('garfield', 'facts.json')
	
	print('Lasagne Time!')
	
	# num = randint(140916, 190825)
	
	# url = f'https://www.bgreco.net/garfield/daily/{num}.png'
	
	fact = choice(garfieldFacts)
	
	await ctx.send(fact)

@bot.command()
async def pancake(ctx):
	print('Pancake Time!')
	
	filename = 'pancake.jpg'
	await ctx.send(file=File(filename))

@bot.command()
async def fuck(ctx):
	messages = ['OwO', 'UwU', '!w!', '>~<']
	await ctx.send(choice(messages))

@bot.command()
async def happy(ctx):
	global positivityLines
	
	print('Positivity Time!')
	
	if len(positivityLines) == 0:
		positivityLines = getQuotes('positivity', 'lines.json')
	
	pic = getRandomImage('positivity')
	
	await ctx.send(file=File(pic))
	await ctx.send(choice(positivityLines))

##############

@bot.command()
async def help(ctx):
	print('I\'m helping!')
	await ctx.send('''Hi there, my name\'s PathBot.
    
I can be used to print out spell descriptions, since Paizo are pretty bad at making playbooks.

I've also got some secret functionality!

To use me, type `!spell [spell name]`.
E.g. `!spell Prestidigitation` or `!spell magic missile`

Spell names are case-insensitive, but punctuation still matters.

I can handle very basic spelling mistakes!

Have a good day!
''')

def getType(spellType):
	result = 'focus spell' if spellType == 'focus' else spellType
	
	return result.title()

def getBasics(spell):
	type = getType(spell['type'])
	
	traits = ', '.join([trait.title() for trait in spell['traits']])
	
	components = []
	components.append(f'''
```Name: {spell['name']}
Level: {spell['level']}
Traits: {traits}
Rarity: {spell['rarity'].title()}

{spell['name']} is a {spell['level']}{getSuffix(spell['level'])} level {spell['rarity']} {type}.
''')
	
	if 'range' in spell and 'area' in spell and 'target' in spell:
		components.append(f'It has a range of {spell["range"]}, an area of effect of {spell["area"]}, and it targets {spell["target"]}.')
	elif 'range' in spell and 'area' in spell:
		components.append(f'It has a range of {spell["range"]} and an area of effect of {spell["area"]}.')
	elif 'range' in spell and 'target' in spell:
		components.append(f'It has a range of {spell["range"]} and targets {spell["target"]}.')
	elif 'area' in spell and 'target' in spell:
		components.append(f'It has an area of effect of {spell["area"]}, and targets {spell["target"]}.')
	elif 'range' in spell:
		components.append(f'It has a range of {spell["range"]}.')
	elif 'area' in spell:
		components.append(f'It has an area of effect of {spell["area"]}.')
	elif 'target' in spell:
		components.append(f'It targets {spell["target"]}')
	
	if 'duration' in spell:
		components.append(f'''
It has a duration of {spell["duration"]}.''')
	
	componentTokens = []
	for component in spell['components']:
		if component.lower() == 'v':
			componentTokens.append('verbal')
		elif component.lower() == 's':
			componentTokens.append('somatic')
		elif component.lower() == 'm':
			componentTokens.append('material')
		else:
			componentTokens.append(component)
	
	components.append(f'''
The {type} has the following components: {', '.join(sorted(componentTokens))}
Casting the {type} requires {spell['actions']}
''')
	
	components.append('```')
	
	return '\n'.join(components)

def getSubsections(spell):
	components = []
	subsections = spell['subsections']
	tempComponents = []
	
	for subsectionName, subData in subsections.items():
		tempComponents = []
		tempComponents.append('```')
		
		if subsectionName == 'disease':
			for key, effect in subData['content'].items():
				if not fullmatch('Stage [0-9]', key):
					tempComponents.append(f'''
The {type} can inflict the {key} disease: {effect}
''')
				else:
					tempComponents.append(f'''
{key}: {effect}
''')
		
		elif subData['type'] == 'dl':
			tempComponents.append('```')
			for key, effect in subData['content'].items():
				tempComponents.append(f'{key}: {effect}')
			tempComponents.append('```')
		
		elif subData['type'] == 'list':
			for effect in subData['content']:
				tempComponents.append(f'''- {effect}
''')
		
		elif subData['type'] == 'table':
			tempComponents.append('Find the apt entry below: ')
			
			tempComponents.append(
				tabulate(
					subData['content']['rows'],
					headers=subData['content']['headers'],
					tablefmt="github"
				)
			)
		
		tempComponents.append('```')
	
	components.append('\n'.join(tempComponents))
	
	return '\n'.join(components)

def getSaveComponents(spell):
	components = []
	
	components.append('```')
	if spell['saveType'] is not None:
		components.append(f'Affected creatures get a {spell["saveType"].title()} save: ')
		if spell['saveType'].startswith('basic'):
			components.append(f'''A basic save means that if it takes double damage on a crit fail, half damage on a success, and no damage on a crit success.
''')
		else:
			for result, effect in spell['save']['content'].items():
				if result == 'crit':
					result = 'critical success'
				elif result == 'crit-fail':
					result = 'Critical Fail'
				
				components.append(f'''{result.title()}: {effect}
''')
	components.append('```')
	
	return '\n'.join(components)

def getHeightComponents(spell):
	components = []
	components.append('```')
	components.append(f'''The {getType(spell['type'])} can be heightened too:
''')
	for height, effect in spell['heightening'].items():
		components.append(f'''{height}: {effect}
''')
	components.append('```')
	
	return '\n'.join(components)

@bot.command()
async def spell(ctx, *args):
	spellName = ' '.join(args)
	
	print(f'I\'m printing a spell! {spellName}')
	spellName = spellName.lower()
	
	matches = editDistance(spellDict, spellName)
	
	if matches[0][0] is None:
		await ctx.send('Sorry, no matching spell found!')
		return
	else:
		if len(matches) == 1:
			spellName = matches[0][0]
		else:
			message = '''```Did you mean one of these?

'''
			for spellMatch in matches:
				message += f'''- {spellMatch[0].title()}
'''
			
			message += '```'
			
			await ctx.send(message)
			return
	
	if spellName in spellDict:
		spell = spellDict[spellName]
		messageComponents = []
		
		basicComponent = getBasics(spell)
		messageComponents.append(basicComponent)
		
		messageComponents.append(f'```{spell["description"]}```')
		
		if 'subsections' in spell:
			subsectionComponent = getSubsections(spell)
			messageComponents.append(subsectionComponent)
		
		if 'saveType' in spell:
			if spell['saveType'] is not None:
				saveComponent = getSaveComponents(spell)
				messageComponents.append(saveComponent)
		
		if 'heightening' in spell:
			heightComponents = getHeightComponents(spell)
			messageComponents.append(heightComponents)
		
		messageComponents.append(generateFlavour(spellName))
		
		message = ''
		
		i = 0
		for index, component in enumerate(messageComponents):
			if len(message) + len(component) < 2000:
				message += component.rstrip().rstrip('\n')
				if index == len(messageComponents)-1:
					await ctx.send(message)
			else:
				print(f'The message {message} is now at max size.')
				await ctx.send(message)
				message = ''
	
	else:
		await ctx.send('Sorry, I don\'t know that spell! Must not be on my spell list.')

print('About to run!')
bot.run(token, bot=True)

# client = discord.Client()
# client.run(token)