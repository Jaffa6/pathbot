# PathBot

A simple Discord bot I hacked together in a few sleepless hours. 

It's mostly useful for Pathfinder 2e games as it can provide spell descriptions, but it can also provide cute animal pics/facts upon command. 

You'll need to add your own "auth.txt" file with a Discord token in it to run the bot: https://discordapp.com/developers/docs/intro

The endpoints are:

!dog

!cat

!frog

!bird

!fox

!pikachu

!happy (For positivity)

!pancake

!fuck

!help

!spell (Followed by a spell name. E.g. "!spell magic missile")

