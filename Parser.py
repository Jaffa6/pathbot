from dumper import loads, dumps
from Spell import Spell
from constants import dataPath, savePath

spellDict = {}

with open(dataPath) as spellDataFile:
	spellData = loads(spellDataFile.read())
	
	for data in spellData.values():
		spell = Spell(data)
		spellDict[spell.name.lower()] = spell
		
	with open(savePath, 'w') as saveFile:
		jsonSpellList = {
			key: value.__dict__ for key, value in spellDict.items()
		}
		saveFile.write(dumps(jsonSpellList))
		